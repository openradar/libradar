#/bin/bash

working_dir=$PWD

mkdir -p $working_dir/src/proto
protoc -I $working_dir/proto $working_dir/proto/messages.proto --cpp_out $working_dir/src/proto --python_out $working_dir/python