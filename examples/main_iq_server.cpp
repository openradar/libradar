#include <common/common.hpp>
#include <common/util.hpp>
#include <iostream>
#include <network/PushServer.hpp>
#include <radio/LimeIf.hpp>
#include <spdlog/spdlog.h>
#include <dsp/waveform/WaveformFactory.hpp>
#include <dsp/RangeDoppler.hpp>
#include <fstream>

int main(int argc, char *argv[])
{
    std::string configName = "./system.yaml";
    if (argc > 1)
    {
        configName = std::string(argv[1]);
    }

    SPDLOG_INFO("Using config: {}", configName);
    radar::config_t config = std::make_shared<ConfigParser>(configName);
    std::cout << config->getConfig<std::string>("RADIO", "TYPE") << std::endl;

    auto wav = radar::wav::WaveformFactory(config).make();
    auto rd_proc = std::make_unique<radar::proc::RangeDoppler>(config);

    std::ofstream file("wav.fc32", std::ios::binary);
    wav->generate(100000);

    radar::fc32_t *wav_ptr = const_cast<radar::fc32_t *>(wav->get_ptr());
    rd_proc->set_kernel(wav_ptr);

    auto test = wav->get();
    file.write(reinterpret_cast<char *>(test.data()), sizeof(radar::fc32_t) * test.size());
    file.close();

    auto limeif = std::make_unique<radio::LimeIf>(config);
    auto iqServer = std::make_unique<PushServer>(config);

    radar::RXCallback_t callable = std::bind(
        &PushServer::queueFc32IqMsg, iqServer.get(), std::placeholders::_1);

    radar::RXCallback_t rd_process = std::bind(&radar::proc::RangeDoppler::process, rd_proc.get(), std::placeholders::_1);

    limeif->registerRxCallback(callable);
    limeif->registerRxCallback(rd_process);

    limeif->start();
    iqServer->start();

    SPDLOG_INFO("Enter 0 to stop: ");
    CLI::WaitForStop();
    SPDLOG_INFO("Stopping!");

    limeif->stop();
    iqServer->stop();
}