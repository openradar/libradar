#pragma once

#include <condition_variable>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

#include <SoapySDR/Device.hpp>
#include <SoapySDR/Formats.hpp>
#include <SoapySDR/Time.hpp>

#include "RadioCommon.hpp"
#include <common/ConfigParser.hpp>
#include <common/common.hpp>
#include <dsp/waveform/WaveformBase.hpp>

namespace radio
{
class LimeIf
{
public:
    LimeIf(radar::config_t config);
    LimeIf(radar::config_t config, std::shared_ptr<radar::wav::Waveform> waveform);
    ~LimeIf() = default;

    void set_waveform(std::shared_ptr<radar::wav::Waveform> waveform);

    // add callback
    void registerRxCallback(radar::RXCallback_t callback);

    // start main processing threads
    void start();
    void stop();

private:
    void init();
    void configureFrontEnd();

    void send();
    void recv();
    void process();

    void cleanup();
    void close();

    std::shared_ptr<radar::wav::Waveform> wave = nullptr;

    std::thread rxThread;
    std::thread txThread;
    std::thread procThread;

    std::condition_variable radioStateChange;

    std::mutex ctlMutex;
    std::mutex limeAccMutex;
    std::atomic_bool running;
    std::atomic<RADIO_STATE> state;

    radar::console_t console;
    radar::config_t config;
    std::vector<radar::RXCallback_t> rxCallbacks;
    SoapySDR::Device *device;
    std::unique_ptr<TXRX_SCHEDULE> sched;
};
}; // namespace radio