#include "LimeIf.hpp"

radio::LimeIf::LimeIf(radar::config_t config) : config(config)
{
    console = spdlog::stdout_color_mt("RADIO");
    console->set_level(static_cast<spdlog::level::level_enum>(
        config->getConfig<int>("RADIO", "LOG_LEVEL")));
    running = false;

    console->info("Initializing the radio");
    try
    {
        init();
    }
    catch (const std::exception &e)
    {
        console->info("Failed to intialze the device! Cleaning up and exiting!!");
        cleanup();
        throw e;
    };
}

radio::LimeIf::LimeIf(radar::config_t config, std::shared_ptr<radar::wav::Waveform> waveform) : config(config)
{
    console = spdlog::stdout_color_mt("RADIO");
    console->set_level(static_cast<spdlog::level::level_enum>(
        config->getConfig<int>("RADIO", "LOG_LEVEL")));
    running = false;

    console->info("Initializing the radio");
    try
    {
        init();
    }
    catch (const std::exception &e)
    {
        console->info("Failed to intialze the device! Cleaning up and exiting!!");
        cleanup();
        throw e;
    };

    wave = waveform;
}

void radio::LimeIf::init()
{
    // Device list
    std::string hwType = config->getConfig<std::string>("RADIO", "TYPE");
    device = SoapySDR::Device::make();

    if (device == nullptr)
    {
        console->error("No device found");
    }

    console->info("Got device, configuring");
    configureFrontEnd();
}

void radio::LimeIf::registerRxCallback(radar::RXCallback_t callback)
{
    rxCallbacks.push_back(callback);
}

void radio::LimeIf::set_waveform(std::shared_ptr<radar::wav::Waveform> waveform)
{
    wave = waveform;
}

void radio::LimeIf::start()
{
    running = true;

    console->info("Starting processing thread");
    procThread = std::thread(std::bind(&radio::LimeIf::process, this));
}

void radio::LimeIf::stop()
{
    console->info("Stopping radio");
    running = false;

    console->debug("Joining radio thread");
    procThread.join();

    cleanup();
    console->info("Done");
}

void radio::LimeIf::cleanup()
{
    console->info("Cleaning up");

    // close
    close();
}

void radio::LimeIf::close()
{
    if (device != nullptr)
    {
        console->info("Closing device");
        SoapySDR::Device::unmake(device);
    }
}

void radio::LimeIf::configureFrontEnd()
{
    const double cf = config->getConfig<double>("RADIO", "FREQUENCY");
    const double rate = config->getConfig<double>("RADIO", "SAMPLE_RATE");
    const unsigned int txGain =
        config->getConfig<unsigned int>("RADIO", "TX_GAIN");
    const unsigned int rxGain =
        config->getConfig<unsigned int>("RADIO", "RX_GAIN");

    // set sample rate
    console->info("Setting RX rate: {} Msps, TX rate {} Msps", rate / 1e6,
                  rate / 1e6);
    device->setSampleRate(SOAPY_SDR_RX, LMS_CH_0, rate);
    device->setSampleRate(SOAPY_SDR_TX, LMS_CH_0, rate);

    const double actualRXRate = device->getSampleRate(SOAPY_SDR_RX, LMS_CH_0);
    const double actualTXRate = device->getSampleRate(SOAPY_SDR_RX, LMS_CH_0);
    console->info("Actual RX rate: {} Msps, Actual TX rate: {} Msps",
                  actualRXRate / 1e6, actualTXRate / 1e6);

    // set center frequency
    console->info("Setting RX LO: {} MHz, TX LO {} MHz", cf / 1e6, cf / 1e6);
    device->setFrequency(SOAPY_SDR_RX, LMS_CH_0, cf);
    device->setFrequency(SOAPY_SDR_TX, LMS_CH_0, cf);

    const double actualRXLO = device->getFrequency(SOAPY_SDR_RX, LMS_CH_0);
    const double actualTXLO = device->getFrequency(SOAPY_SDR_TX, LMS_CH_0);
    console->info("Actual RX LO: {} MHz, Actual TX LO: {} MHz", actualRXLO / 1e6,
                  actualTXLO / 1e6);

    // set gains
    console->info("Setting RX gain: {} dB, TX gain {} dB", rxGain, txGain);
    device->setGain(SOAPY_SDR_RX, LMS_CH_0, rxGain);
    device->setGain(SOAPY_SDR_TX, LMS_CH_0, txGain);

    const unsigned int actualRXGain = device->getGain(SOAPY_SDR_RX, LMS_CH_0);
    const unsigned int actualTXGain = device->getGain(SOAPY_SDR_TX, LMS_CH_0);
    console->info("Actual RX LO: {} dB, Actual TX LO: {} dB", actualRXGain,
                  actualTXGain);

    // get tx rx schedule
    float pulseLen_us = config->getConfig<float>("WAVEFORM", "PULSE_LEN_US");
    float prf = config->getConfig<float>("WAVEFORM", "PRF_HZ");
    sched = std::make_unique<radio::TXRX_SCHEDULE>(pulseLen_us, prf, rate);

    // generate waveform
    if (wave != nullptr)
    {
        wave->generate(sched->txDewll_samples);
    }

    console->info("RADAR Schedule:: PRI: {} us, TX dwell: {} us, RX dwell: {} us",
                  sched->pri_us, sched->txDwell_us, sched->rxDwell_us);
}

void radio::LimeIf::process()
{
    // time vars
    long long txTime = 0;
    long long rxTime = 0;
    long timeOut = 100 * 1000; // 100 ms in us
    int rxFlags = SOAPY_SDR_HAS_TIME;
    int txFlags = 0;
    bool firstTransmit = true;

    // setup streams
    console->info("Getting streams");
    SoapySDR::Stream *txStream;
    txStream = device->setupStream(SOAPY_SDR_TX, SOAPY_SDR_CF32, {0});

    SoapySDR::Stream *rxStream;
    rxStream = device->setupStream(SOAPY_SDR_RX, SOAPY_SDR_CF32, {0});

    // TODO: use a multiple of the MTU for streaming
    auto const streamMTU = device->getStreamMTU(rxStream);
    console->debug("Stream MTU size: {} samples", streamMTU);

    console->info("Allocating buffers");
    radar::AlignedVector<radar::fc32_t> recvBuff(sched->rxDewll_samples);
    std::vector<void *> rxBuffs(1);
    rxBuffs[0] = recvBuff.data();

    std::vector<void *> txBuffs(1);
    if (wave == nullptr)
    {
        std::vector<radar::fc32_t> txBuff(sched->txDewll_samples, 1);
        txBuffs[0] = txBuff.data();
    }
    else
    {
        txBuffs[0] = const_cast<radar::fc32_t *>(wave->get_ptr());
    }

    // start RX buffer and receive some scratch data
    console->info("Activating RX stream");
    device->activateStream(rxStream);

    console->info("Activating TX stream");
    device->activateStream(txStream);

    console->info("Syncing time");
    int wut = 0;
    int ret = device->readStream(
        rxStream, rxBuffs.data(),
        sched->rxDewll_samples > streamMTU ? streamMTU : sched->rxDewll_samples,
        wut, rxTime, timeOut);

    // First RX one second in the future
    rxTime = device->getHardwareTime() + 1000000000;
    console->info("RX Time: {}", rxTime);

    while (running)
    {
        // receive
        int ret = device->readStream(
            rxStream, rxBuffs.data(),
            sched->rxDewll_samples > streamMTU ? streamMTU : sched->rxDewll_samples,
            rxFlags, rxTime, timeOut);

        rxTime += sched->txDwell_us * 1e3;
        // console->info("RX Time: {}", rxTime);

        for (const auto &callback : rxCallbacks)
        {
            callback(recvBuff);
        }

        // transmit
        int ret0 =
            device->writeStream(txStream, txBuffs.data(), sched->txDewll_samples,
                                txFlags, txTime, timeOut);
    }
    device->deactivateStream(txStream);
    device->closeStream(txStream);

    device->deactivateStream(rxStream);
    device->closeStream(rxStream);
}