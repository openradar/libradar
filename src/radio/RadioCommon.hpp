#pragma once
#include <memory>

namespace radio {
static constexpr int LMS_CH_0 = 0;
static constexpr int LMS_CH_1 = 1;

static constexpr int LimeSDR_Mini = 1;
static constexpr int LimeNET_Micro = 2;
static constexpr int LimeSDR_USB = 3;

enum class RADIO_STATE : uint8_t { TRANSMITTING, RECEIVING, WAITING };

class TXRX_SCHEDULE {
public:
  TXRX_SCHEDULE() = delete;
  TXRX_SCHEDULE(float pulseLen_us, float prf, float sampleRate);

  float prf;
  float pri_us;
  float rxDwell_us;
  size_t rxDewll_samples;

  float txDwell_us;
  size_t txDewll_samples;
};

inline TXRX_SCHEDULE::TXRX_SCHEDULE(float pulseLen_us, float prf,
                                    float sampleRate) {
  this->prf = prf;
  this->pri_us = 1e6 / prf;
  txDwell_us = pulseLen_us;
  txDewll_samples = (size_t)(txDwell_us * sampleRate / 1e6);

  rxDwell_us = (pri_us - txDwell_us);
  rxDewll_samples = (size_t)(rxDwell_us * sampleRate / 1e6);
};
} // namespace radio