#pragma once
#include <complex>
#include <functional>
#include <memory>
#include <vector>
#include <volk/volk.h>
#include <limits>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#include "ConfigParser.hpp"
#include <chrono>

namespace radar
{
/**
 * @brief AlignedAllocator adapted from: https://en.cppreference.com/w/cpp/named_req/Allocator
 * 
 * @tparam T 
 */
template <class T>
struct AlignedAllocator
{
    typedef T value_type;
    AlignedAllocator() = default;
    template <class U>
    constexpr AlignedAllocator(const AlignedAllocator<U> &) noexcept {}
        [[nodiscard]] T *allocate(std::size_t n)
    {
        if (n > std::numeric_limits<std::size_t>::max() / sizeof(T))
            throw std::bad_alloc();
        auto alignment = volk_get_alignment();
        if (auto p = static_cast<T *>(volk_malloc(n * sizeof(T), alignment)))
            return p;
        throw std::bad_alloc();
    }
    void deallocate(T *p, std::size_t) noexcept { volk_free(p); }
};
template <class T, class U>
bool operator==(const AlignedAllocator<T> &, const AlignedAllocator<U> &) { return true; }
template <class T, class U>
bool operator!=(const AlignedAllocator<T> &, const AlignedAllocator<U> &) { return false; }

using config_t = std::shared_ptr<ConfigParser>;
using fc32_t = std::complex<float>;
using sc16_t = std::complex<int16_t>;
using console_t = std::shared_ptr<spdlog::logger>;

template <class T>
using AlignedVector = std::vector<T, AlignedAllocator<T>>;
using RXCallback_t = std::function<void(AlignedVector<fc32_t> &)>;

auto next_pow_2 = [](auto in) { return 1 << static_cast<int>(std::floor(std::log2(in)) + 1); };
}; // namespace radar