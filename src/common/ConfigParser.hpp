#pragma once

#include <iostream>
#include <mutex>
#include <string>
#include <yaml-cpp/yaml.h>

class ConfigParser {
public:
  ConfigParser(std::string configFileName);

  template <typename T>
  T getConfig(std::string sectionName, std::string varName);

  template <typename T>
  T getConfig(std::string sectionName, std::string varName, const T defaultVal);

private:
  std::mutex readCtl;
  YAML::Node config;
};

inline ConfigParser::ConfigParser(std::string configFileName) {
  config = YAML::LoadFile(configFileName);
}

template <typename T>
T ConfigParser::getConfig(std::string sectionName, std::string varName) {
  T item;
  std::lock_guard<std::mutex> lk(readCtl);
  try {
    item = std::move(config[sectionName][varName].as<T>());
  } catch (YAML::Exception e) {
    std::cerr << "Failed to parse config. Section name: " << sectionName
              << ", var name: " << varName << std::endl;
    std::cerr << e.what();
    throw e;
  }
  return item;
}

template <typename T>
T ConfigParser::getConfig(std::string sectionName, std::string varName,
                          const T defaultVal) {
  T item;
  std::lock_guard<std::mutex> lk(readCtl);
  try {
    item = std::move(config[sectionName][varName].as<T>());
  } catch (YAML::Exception e) {
    item = defaultVal;
  }
  return item;
}
