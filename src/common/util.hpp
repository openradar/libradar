#pragma once
#include <iostream>

namespace CLI
{
inline void WaitForStop()
{
    int x = 1;
    while (x != 0)
    {
        std::cin >> x;
    }
}
}; // namespace CLI