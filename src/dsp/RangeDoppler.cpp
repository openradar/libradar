#include <dsp/RangeDoppler.hpp>
#include <common/common.hpp>
#include <iostream>

radar::proc::RangeDoppler::RangeDoppler(radar::config_t config)
{
    // read from cfg
    double freqres = config->getConfig<double>("PROCESSING", "FREQ_RES_HZ");
    double sample_rate = config->getConfig<double>("RADIO", "SAMPLE_RATE");
    double time_win = config->getConfig<double>("PROCESSING", "TIME_WINDOW_US");

    // The FFT size controls our time resolution
    fft_size = radar::next_pow_2(sample_rate * time_win * 1e-6);

    // Get configured frequency resolution
    // This calculation is not exact...I should probably fix that at some point
    num_freq_bins = std::round(config->getConfig<double>("PROCESSING", "PROCESSING_BW_HZ") / freqres);
    num_shift_bins = 1;
    std::cout << num_freq_bins << "," << num_shift_bins << "\n";

    // resize the buffers
    scratch.resize(fft_size);
    rdmap.resize(num_freq_bins);
    kernel.resize(num_freq_bins);
    for (int i = 0; i < num_freq_bins; ++i)
    {
        rdmap[i].resize(fft_size);
        kernel[i].resize(fft_size);
    }

    auto out = reinterpret_cast<fftwf_complex *>(scratch.data());
    forward_plan = fftwf_plan_dft_1d(fft_size, out, out, FFTW_FORWARD, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
    inverse_plan = fftwf_plan_dft_1d(fft_size, out, out, FFTW_BACKWARD, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
}

radar::proc::RangeDoppler::~RangeDoppler()
{
    fftwf_destroy_plan(forward_plan);
    fftwf_destroy_plan(inverse_plan);
}

void radar::proc::RangeDoppler::set_kernel(radar::fc32_t *kern)
{
    // TODO: Zero pad the kernel if needed
    // conjugate the kernel
    volk_32fc_conjugate_32fc(kernel[0].data(), kern, fft_size);
    std::reverse(kernel[0].begin(), kernel[0].end());

    // FFT
    auto out = reinterpret_cast<fftwf_complex *>(kernel[0].data());

    fftwf_plan tmp_plan = fftwf_plan_dft_1d(fft_size, out, out, FFTW_FORWARD, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
    fftwf_execute(tmp_plan);
    fftwf_destroy_plan(tmp_plan);

    // build frequency map by shifting the kernel
    for (int i = 1; i < num_freq_bins; ++i)
    {
        std::copy(kernel[i - 1].end() - num_shift_bins, kernel[i - 1].end(), kernel[i].begin());
        std::copy(kernel[i - 1].begin(), kernel[i - 1].end() - num_shift_bins - 1, kernel[i].begin() + num_shift_bins + 1);
    }
}

void radar::proc::RangeDoppler::process(const radar::AlignedVector<radar::fc32_t> &iq) noexcept
{
    if (iq.size() > scratch.size())
    {
        scratch.resize(iq.size());

        // remake FFTW plans
        fftwf_destroy_plan(forward_plan);
        fftwf_destroy_plan(inverse_plan);
        auto out = reinterpret_cast<fftwf_complex *>(scratch.data());
        forward_plan = fftwf_plan_dft_1d(fft_size, out, out, FFTW_FORWARD, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
        inverse_plan = fftwf_plan_dft_1d(fft_size, out, out, FFTW_BACKWARD, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
    }
    std::copy(iq.begin(), iq.end(), scratch.begin());

    fftwf_execute(forward_plan);

    std::cout << fft_size << "\n";

    // lets do this
    // Super naive method. i.e. the best one around
    for (int i = 0; i < num_freq_bins; ++i)
    {
        volk_32fc_x2_multiply_32fc(rdmap[i].data(), kernel[i].data(), scratch.data(), fft_size);

        auto out = reinterpret_cast<fftwf_complex *>(rdmap[i].data());
        fftwf_execute_dft(inverse_plan, out, out);
    }

    // pass onto detector and then trackerzz and then gui
};