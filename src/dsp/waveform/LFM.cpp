#include <dsp/waveform/LFM.hpp>
#include <iostream>

constexpr char radar::wav::LFM::desc[];

radar::wav::LFM::LFM(radar::config_t config)
{
    parseCfg(config);
}

radar::wav::LFM::~LFM()
{
    // nothing to do
}

void radar::wav::LFM::parseCfg(radar::config_t config)
{
    double cf = config->getConfig<double>("WAVEFORM", "CF");
    double bw = config->getConfig<double>("WAVEFORM", "BW_HZ");
    start_freq = cf - bw / 2;
    stop_freq = cf + bw / 2;
    pulse_len = 1e-6 * config->getConfig<double>("WAVEFORM", "PULSE_LEN_US");
    sample_rate = config->getConfig<double>("RADIO", "SAMPLE_RATE");
}

void radar::wav::LFM::reset()
{
    // TODO
}

void radar::wav::LFM::clear()
{
    //TODO
}

void radar::wav::LFM::generate(int num_samples)
{
    if (num_samples <= 0)
        num_samples = (pulse_len * sample_rate);
    buff.resize(num_samples);
    double bandwidth = (stop_freq - start_freq) / (2 * pulse_len);
    std::cout << "chaip: " << bandwidth << std::endl;
    int max_samples = pulse_len * sample_rate;
    for (int i = 0; i < num_samples; ++i)
    {
        double t = (i % max_samples) / sample_rate;
        double phase = 2.0 * M_PIf64 * (start_freq * t + bandwidth * (t * t));
        buff[i] = radar::fc32_t(std::cos(phase), std::sin(phase));
    }
}

const radar::AlignedVector<radar::fc32_t> radar::wav::LFM::get(size_t offset) const
{
    return {buff.begin() + offset, buff.end()};
}

const radar::fc32_t *radar::wav::LFM::get_ptr(size_t offset) const
{
    return &buff[offset];
}
