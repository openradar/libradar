#include <dsp/waveform/WaveformFactory.hpp>
#include <string>

radar::wav::WaveformFactory::WaveformFactory(config_t config)
{
    this->config = config;
}

std::shared_ptr<radar::wav::Waveform> radar::wav::WaveformFactory::make() const
{
    std::string wtype = config->getConfig<std::string>("WAVEFORM", "TYPE");

    if (wtype.compare(radar::wav::LFM::desc) == 0)
        return std::make_shared<radar::wav::LFM>(config);
}