#pragma once
#include <dsp/waveform/WaveformBase.hpp>
#include <common/common.hpp>
#include <vector>

namespace radar
{
namespace wav
{
class LFM : public Waveform
{

public:
    constexpr static char desc[] = "LFM";
    LFM(config_t config);
    ~LFM();

    virtual const AlignedVector<radar::fc32_t> get(size_t offset = 0) const;
    virtual const radar::fc32_t *get_ptr(size_t offset = 0) const;
    virtual void reset();
    virtual void generate(int num_samples = 0);
    virtual void clear();

private:
    void parseCfg(config_t config);

    double sample_rate;
    double start_freq;
    double stop_freq;
    double pulse_len;

    AlignedVector<fc32_t> buff;
};
}; // namespace wav
}; // namespace radar