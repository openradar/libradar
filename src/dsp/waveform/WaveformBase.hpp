#pragma once
#include <common/common.hpp>

namespace radar
{
namespace wav
{
class Waveform
{
public:
    ~Waveform() = default;
    virtual const AlignedVector<radar::fc32_t> get(size_t offset = 0) const = 0;
    virtual const radar::fc32_t *get_ptr(size_t offset = 0) const = 0;
    virtual void reset() = 0;
    virtual void generate(int num_samples = 0) = 0;
    virtual void clear() = 0;
};
}; // namespace wav
}; // namespace radar