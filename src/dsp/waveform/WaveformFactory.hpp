#pragma once
#include <map>
#include <string>
#include <common/common.hpp>
#include <dsp/waveform/LFM.hpp>

namespace radar
{
namespace wav
{
class WaveformFactory
{
public:
    WaveformFactory(config_t config);
    ~WaveformFactory() = default;

    std::shared_ptr<Waveform> make() const;

private:
    config_t config;
};
}; // namespace wav
}; // namespace radar