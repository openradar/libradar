#pragma once
#include <common/common.hpp>
#include <vector>
#include <fftw3.h>

namespace radar
{
namespace proc
{
class RangeDoppler
{
public:
    RangeDoppler(config_t config);
    ~RangeDoppler();

    void process(const AlignedVector<fc32_t> &iq) noexcept;
    void set_kernel(fc32_t *kern);

    std::vector<AlignedVector<fc32_t>> rdmap;

private:
    void init();

    int fft_size;
    int num_freq_bins;
    int num_shift_bins;
    fftwf_plan forward_plan;
    fftwf_plan inverse_plan;
    std::vector<AlignedVector<fc32_t>> kernel;
    AlignedVector<fc32_t> scratch;
};
} // namespace proc
} // namespace radar
