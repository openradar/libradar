#include "PushServer.hpp"

PushServer::PushServer(radar::config_t config) : config(config)
{
    console = spdlog::stdout_color_mt("NETWORK");
    msgQueue = std::deque<radar::Wrapper>();
}

void PushServer::start()
{
    // start the server thread
    running = true;
    netThread = std::thread(std::bind(&PushServer::server, this));
}

void PushServer::stop()
{
    running = false;
    netThread.join();
}

void PushServer::queueFc32IqMsg(radar::AlignedVector<radar::fc32_t> &iq)
{
    auto wrapper = radar::Wrapper();
    auto iqMsg = radar::IQ_FC32();
    iqMsg.set_id(0);

    for (const auto &val : iq)
    {
        iqMsg.add_iq(val.real());
        iqMsg.add_iq(val.imag());
    }

    wrapper.set_message_type(iqMsg.GetTypeName());
    wrapper.set_message(iqMsg.SerializeAsString());

    // grab a lock and push into queue
    {
        std::lock_guard<std::mutex> lk(ctlMut);
        msgQueue.push_back(wrapper);
    }
}

void PushServer::queueSc16IqMsg(std::vector<radar::sc16_t> &iq)
{
    auto wrapper = radar::Wrapper();
    auto iqMsg = radar::IQ_SC16();
    iqMsg.set_id(0);

    for (const auto &val : iq)
    {
        iqMsg.add_iq(val.real());
        iqMsg.add_iq(val.imag());
    }

    wrapper.set_message_type(iqMsg.GetTypeName());
    wrapper.set_message(iqMsg.SerializeAsString());

    // grab a lock and push into queue
    {
        std::lock_guard<std::mutex> lk(ctlMut);
        msgQueue.push_back(wrapper);
    }
}

void PushServer::server()
{
    // read config
    std::string ip = config->getConfig<std::string>("NETWORK", "IP", "127.0.0.1");
    std::string port = config->getConfig<std::string>("NETWORK", "PORT", "5558");
    int timeout = config->getConfig<int>("NETWORK", "TIMEOUT_MS", 100);

    // set up server stuff
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_PUSH);

    std::string address = "tcp://" + ip + ":" + port;
    console->info("Binding socket at: {}", address);
    socket.bind(address);
    console->info("Bound");
    socket.setsockopt(ZMQ_SNDTIMEO, timeout);

    while (running)
    {
        radar::Wrapper msg;
        bool doSend = false;
        {
            std::lock_guard<std::mutex> lk(ctlMut);
            if (msgQueue.size())
            {
                msg = std::move(msgQueue.front());
                msgQueue.pop_front();
                doSend = true;
            }
        }

        if (doSend)
        {
            auto msgStr = msg.SerializeAsString();

            // build zmq message
            zmq::message_t zmqMsg(msgStr.c_str(), msgStr.size());

            // send data
            socket.send(zmqMsg);
        }
        else
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    }
}