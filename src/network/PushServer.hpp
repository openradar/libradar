#pragma once
#include <common/common.hpp>
#include <deque>
#include <mutex>
#include <proto/messages.pb.h>
#include <thread>
#include <zmq.hpp>

class PushServer
{
public:
    PushServer(radar::config_t config);
    ~PushServer() = default;

    void start();
    void stop();

    // queue messages
    void queueFc32IqMsg(radar::AlignedVector<radar::fc32_t> &iq);
    void queueSc16IqMsg(std::vector<radar::sc16_t> &iq);

    void server();

private:
    radar::config_t config;

    std::thread netThread;
    std::mutex ctlMut;
    std::deque<radar::Wrapper> msgQueue;
    radar::console_t console;

    std::atomic_bool running;
};