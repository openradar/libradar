#include <common/common.hpp>
#include <common/util.hpp>
#include <radio/LimeIf.hpp>
#include <iostream>
#include <spdlog/spdlog.h>

int main(int argc, char *argv[])
{
    std::string configName = "./system.yaml";
    if (argc > 1)
    {
        configName = std::string(argv[1]);
    }

    SPDLOG_INFO("Using config: {}", configName);

    radar::config_t config = std::make_shared<ConfigParser>(configName);
    std::cout << config->getConfig<std::string>("RADIO", "TYPE") << std::endl;
    auto limeif = std::make_unique<radio::LimeIf>(config);
    limeif->start();

    SPDLOG_INFO("Enter 0 to stop: ");
    CLI::WaitForStop();
    SPDLOG_INFO("Stopping!");
    limeif->stop();

    return 0;
}