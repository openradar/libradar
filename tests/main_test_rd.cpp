#include <common/common.hpp>
#include <common/util.hpp>
#include <fstream>
#include <dsp/RangeDoppler.hpp>
#include <dsp/waveform/WaveformFactory.hpp>
#include <volk/volk.h>

radar::AlignedVector<radar::fc32_t> rect(int num_total_samples, int num_on_samples, int offset = 0, radar::fc32_t phase_inc = radar::fc32_t(0, 0))
{
    auto ret = radar::AlignedVector<radar::fc32_t>(num_total_samples, 0);
    auto on = radar::AlignedVector<radar::fc32_t>(num_on_samples, 1);

    if (offset + num_on_samples > num_total_samples)
        offset = num_total_samples - num_on_samples;

    std::copy(on.begin(), on.end(), ret.begin() + offset);

    if (phase_inc != radar::fc32_t(0, 0))
    {
        radar::fc32_t phase = radar::fc32_t(1, 0);
        volk_32fc_s32fc_x2_rotator_32fc(ret.data(), ret.data(), phase_inc, &phase, num_total_samples);
    }

    return ret;
}

int main()
{
    radar::config_t config = std::make_shared<ConfigParser>("../tests/test_config.yaml");
    auto wav = radar::wav::WaveformFactory(config).make();

    auto rdProc = std::make_unique<radar::proc::RangeDoppler>(config);
    wav->generate();

    auto kern = rect(1024, 100);
    auto s1 = rect(1024, 100, 10);
    auto s2 = rect(1024, 100, 100, radar::fc32_t(std::cos(2 * M_PI * 5 / 1024.f), std::sin(2 * M_PI * 5 / 1024.f)));
    auto recv = radar::AlignedVector<radar::fc32_t>(1024);
    volk_32fc_x2_add_32fc(recv.data(), s1.data(), s2.data(), 1024);

    rdProc->set_kernel(kern.data());
    rdProc->process(recv);

    auto file = std::ofstream("rdmap.bin", std::ios::binary);

    auto rdRef = rdProc->rdmap;
    int32_t num_rows = rdRef.size();
    int32_t num_cols = rdRef[0].size();
    file.write(reinterpret_cast<char *>(&num_rows), sizeof(int32_t));
    file.write(reinterpret_cast<char *>(&num_cols), sizeof(int32_t));
    for (auto &rd : rdRef)
    {
        file.write(reinterpret_cast<char *>(rd.data()), num_cols * sizeof(radar::fc32_t));
    }
    file.close();

    return 0;
}