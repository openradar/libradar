import argparse
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig

def main(fname):
    data = np.fromfile(fname, np.complex64)

    # b = sig.firwin(128, 2.0*5500/1e6)

    # data = sig.lfilter(b, [1], data)

    plt.figure()
    plt.plot(data.real)

    plt.figure()
    plt.psd(data, 2**17, 10e6)
    plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--fname', '-f', type=str, help='file name')

    args = parser.parse_args()
    main(args.fname)