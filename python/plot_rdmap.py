import argparse
import numpy as np
import scipy.signal as scisig
import matplotlib.pyplot as plt

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', type=str, required=True)

    args = parser.parse_args()

    with open(args.file) as fid:
        num_rows = np.fromfile(fid, np.int32, count=1)[0]
        num_cols = np.fromfile(fid, np.int32, count=1)[0]
        data = np.fromfile(fid, np.complex64)

    print(num_rows, num_cols, data[:10])
    data = np.reshape(np.abs(data)/1024, (num_rows, num_cols))

    tdata = np.append(np.ones(100, dtype=np.complex64), np.zeros(900, dtype=np.complex64))
    tdata2 = np.append(np.zeros(10, dtype=np.complex64), np.ones(100, dtype=np.complex64))
    tdata2 = np.append(tdata2, np.zeros(1000-10-100, dtype=np.complex64))
    corr = scisig.fftconvolve(tdata2, tdata.conj()[::-1], 'full')

    plt.figure()
    plt.plot(np.abs(tdata))
    plt.plot(np.abs(tdata2))

    plt.figure()
    plt.plot(np.abs(corr[1000:]))
    plt.plot(data[0])

    plt.figure()    
    plt.imshow(20*np.log10(data), aspect='auto', extent=[0, 1e-6*3e8, 10*(1e6/1024), 0], vmin=0)
    plt.title('Range - Doppler')
    plt.ylabel('Doppler (Hz)')
    plt.xlabel('Range (m)')
    plt.show()