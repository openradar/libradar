import argparse
import sys
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
from multiprocessing import Process, Queue, Event
from threading import Thread

from PullServer import zmq_pull_client

class radar_ui:
    def __init__(self):
        self.app = QtGui.QApplication([])
        self.app.aboutToQuit.connect(self.close)

        self._win = pg.GraphicsLayoutWidget()
        self._win.setWindowTitle('pyqtgraph example: PanningPlot')
        self.plt = self._win.addPlot()
        self.curve = self.plt.plot()
        self._win.show()

        self.queue = Queue()
        self.client = zmq_pull_client(5000, self.update)
        self.running = Event()

        self.client.start()
        self.freqs = np.linspace(-1e6/2, 1e6/2, 1024)
        self.plt.setYRange(-1, 1, padding=0)

    def close(self):
        self.client.stop_client()
        sys.exit(0)

    def update(self, iq):
        iq = iq[::2] + 1j*iq[1::2]
        # iq_fft = 20*np.log10(np.abs(np.fft.fftshift(np.fft.fft(iq, 1024))))
        self.curve.setData(np.arange(len(iq))/1e6, np.abs(iq))
        

def main():
    radar_ui()

    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

    print("Here")
    return 0


if __name__ == "__main__":
    sys.exit(main())