import zmq
from multiprocessing import Process, Event, Queue
from threading import Thread
import messages_pb2
import time
import numpy as np

class zmq_pull_client(Thread):
    def __init__(self, port, callback, **kwargs):
        super(zmq_pull_client, self).__init__(**kwargs)
        
        self._callback = callback
        self.running = Event()

        self.port = "tcp://127.0.0.1:" + str(port)

    def stop_client(self):
        self.running.set()

    def run(self):
        print("Starting rx thread")
        context = zmq.Context()
        sock = context.socket(zmq.PULL)
        sock.setsockopt(zmq.RCVTIMEO, 1000)
        sock.connect(self.port)
        print("Connected to server")

        while not self.running.is_set():
            try:
                msg = sock.recv()
            except zmq.Again:
                time.sleep(0.1)
                continue

            wrapper = messages_pb2.Wrapper()
            wrapper.ParseFromString(msg)

            iq_msg = getattr(messages_pb2, wrapper.message_type.split('.')[-1])()
            iq_msg.ParseFromString(wrapper.message)
            self._callback(np.array(iq_msg.iq))

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import numpy as np

    fig, ax = plt.subplots(1)
    # fig2, ax2 = plt.subplots(1)

    waterfall_img = np.zeros((100, 6*1024))
    fft_data = np.zeros(6*1024)
    thresh_data = np.zeros(6*1024)
    row_count = 0
    chan_count = 0

    fft_dict = {}

    queue = Queue()
    client = zmq_pull_client(5000, queue)

    client.start()

    dets = []
    amps = []

    while True:
        data = queue.get(block=True)
        # if not (data.freq_hz in fft_dict.keys()):
        #     fft_dict.update({data.freq_hz, (20*np.log10(np.fft.fftshift(data.fftMag)), 20*np.log10(np.fft.fftshift(data.thresh)))})

        # fft_dict[data.freq_hz] = (20*np.log10(np.fft.fftshift(data.fftMag)), 20*np.log10(np.fft.fftshift(data.thresh)), data.det)

        # ax[0].plot(20*np.log10(np.fft.fftshift(data.fftMag)))
        # ax[0].plot(20*np.log10(np.fft.fftshift(data.thresh)))
        # for det in data.det:
        #     dets.append(data.freq_hz + det.freq_hz)
        #     amps.append(20*np.log10(det.amp))
        
        # ax[1].scatter(dets, amps)
        iq = np.array(data.iq)
        iq = iq[::2] + 1j*iq[1::2]

        iq_fft = np.fft.fftshift(np.fft.fft(iq))
        freqs = np.linspace(-30e6/2, 30e6/2, len(iq_fft))
        ax.plot(freqs, np.abs(iq_fft))

        # ax2.imshow(waterfall_img, aspect='auto')

        # for key, value in fft_dict.items():
        #     ax[0].plot(value[0])
        #     ax[0].plot(value[1])
        #     for det in value[2]:
        #         ax[1].plot(det.freq_hz, 1, 'r*')
        plt.pause(0.000001)
        ax.cla()
        
