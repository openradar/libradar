cmake_minimum_required(VERSION 3.8)
project(libradar CXX C)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -march=native -mtune=native")

########################################################################
# Find boost
########################################################################
MESSAGE(STATUS "Configuring Boost C++ Libraries...")

SET(BOOST_REQUIRED_COMPONENTS
    thread
    system
)

if(UNIX AND EXISTS "/usr/lib64")
    list(APPEND BOOST_LIBRARYDIR "/usr/lib64") #fedora 64-bit fix
endif(UNIX AND EXISTS "/usr/lib64")

set(Boost_ADDITIONAL_VERSIONS
    "1.35.0" "1.35" "1.36.0" "1.36" "1.37.0" "1.37" "1.38.0" "1.38" "1.39.0" "1.39"
    "1.40.0" "1.40" "1.41.0" "1.41" "1.42.0" "1.42" "1.43.0" "1.43" "1.44.0" "1.44"
    "1.45.0" "1.45" "1.46.0" "1.46" "1.47.0" "1.47" "1.48.0" "1.48" "1.49.0" "1.49"
    "1.50.0" "1.50" "1.51.0" "1.51" "1.52.0" "1.52" "1.53.0" "1.53" "1.54.0" "1.54"
    "1.55.0" "1.55" "1.56.0" "1.56" "1.57.0" "1.57" "1.58.0" "1.58" "1.59.0" "1.59"
    "1.60.0" "1.60" "1.61.0" "1.61" "1.62.0" "1.62" "1.63.0" "1.63" "1.64.0" "1.64"
    "1.65.0" "1.65" "1.66.0" "1.66" "1.67.0" "1.67" "1.68.0" "1.68" "1.69.0" "1.69"
)

find_package(Boost COMPONENTS ${BOOST_REQUIRED_COMPONENTS})

if(NOT Boost_FOUND)
    message(FATAL_ERROR "Boost required to compile limesdr")
endif()

########################################################################
# Find LimeSuite
########################################################################
# MESSAGE(STATUS "Configuring LimeSuite C++ Libraries...")
# INCLUDE(FindPkgConfig)
# PKG_CHECK_MODULES(PC_LIMESUITE LimeSuite)
# find_path(LIMESUITE_INCLUDE_DIRS 
#  NAMES LimeSuite.h
#  HINTS ${PC_LIMESUITE_INCLUDEDIR}/lime
#  PATHS ${LIMESUITE_PKG_INCLUDE_DIRS}
#        /usr/include/lime
#        /usr/local/include/lime
# )
# find_library(LIMESUITE_LIB 
#  NAMES LimeSuite limesuite
#  HINTS ${PC_LIMESUITE_LIBDIR}
#  PATHS ${LIMESDR_PKG_LIBRARY_DIRS}
#        /usr/lib
#        /usr/local/lib
# )

# message(STATUS "Found LimeSuite: ${LIMESUITE_INCLUDE_DIRS}, ${LIMESUITE_LIB}")

message(STATUS "FINDING SOAPY.")
if(NOT SOAPYSDR_FOUND)

  find_path(SOAPYSDR_INCLUDE_DIRS 
    NAMES Device.h
    PATHS ${SOAPYSDR_PKG_INCLUDE_DIRS}
          /usr/include/SoapySDR
          /usr/local/include/SoapySDR
  )

  find_library(SOAPYSDR_LIBRARIES 
    NAMES SoapySDR
    PATHS ${LIMESDR_PKG_LIBRARY_DIRS}
          /usr/lib
          /usr/local/lib
          /usr/lib/arm-linux-gnueabihf     
  )


if(SOAPYSDR_INCLUDE_DIRS AND SOAPYSDR_LIBRARIES)
  set(SOAPYSDR_FOUND TRUE CACHE INTERNAL "libSOAPYSDR found")
  message(STATUS "Found libSOAPYSDR: ${SOAPYSDR_INCLUDE_DIRS}, ${SOAPYSDR_LIBRARIES}")
else(SOAPYSDR_INCLUDE_DIRS AND SOAPYSDR_LIBRARIES)
  set(SOAPYSDR_FOUND FALSE CACHE INTERNAL "libSOAPYSDR found")
  message(STATUS "libSOAPYSDR not found.")
endif(SOAPYSDR_INCLUDE_DIRS AND SOAPYSDR_LIBRARIES)

mark_as_advanced(SOAPYSDR_LIBRARIES SOAPYSDR_INCLUDE_DIRS)

endif(NOT SOAPYSDR_FOUND)

#########################################################
#yaml cpp
#########################################################
find_package(yaml-cpp REQUIRED)

#########################################################
#spdlog
#########################################################
find_package(spdlog REQUIRED)

#########################################################
#protobuf
#########################################################
find_package(Protobuf REQUIRED)

# set binary dir
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

# compile libraries
add_subdirectory(src)

# compile tests
add_subdirectory(tests)

add_subdirectory(examples)